<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/backend_zone', function(){
    return view('admin.dashboard');
})->middleware(['auth', 'auth.superadmin']);

Route::namespace('Admin')->prefix('backend_zone')->middleware(['auth', 'auth.superadmin'])->name('backend_zone.')->group(function(){
    Route::resource('/users', 'UserController', ['except' => ['show', 'create', 'store']]);
});

Route::namespace('User')->prefix('member_zone')->middleware(['auth', 'auth.member'])->name('member_zone.')->group(function(){
    Route::resource('/profile', 'UserController', ['except' => ['show', 'create', 'store']]);
});

// Route::get('/admin_zone/announcement', function(){
//     return view('admin.announcement');
// });

// Route::get('/admin_zone/member', function(){
//     return view('admin.member');
// });

// Route::get('/admin_zone/staff', function(){
//     return view('admin.staff');
// });

// Route::get('/admin_zone/deposit', function(){
//     return view('admin.deposit');
// });

// Route::get('/admin_zone/withdraw', function(){
//     return view('admin.withdraw');
// });

// Route::get('/admin_zone/bank', function(){
//     return view('admin.bank');
// });

// Route::get('/admin_zone/mutation', function(){
//     return view('admin.mutation');
// });