<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $superAdminRole = Role::where('name', 'superadmin')->first();
        $adminRole = Role::where('name', 'admin')->first();
        $memberRole = Role::where('name', 'member')->first();

        $superAdmin = User::create([
            'name' => 'Anggit Ari Utomo',
            'email' => 'pucukpisang97@gmail.com',
            'password' => bcrypt('anggit12'),
        ]);

        $admin = User::create([
            'name' => 'Indra Apriliansyah',
            'email' => 'indra97@gmail.com',
            'password' => bcrypt('indra12'),
        ]);

        $member = User::create([
            'name' => 'Muhammad Nazrin',
            'email' => 'nazrin97@gmail.com',
            'password' => bcrypt('nazrin12'),
        ]);

        $superAdmin->roles()->attach($superAdminRole);
        $admin->roles()->attach($adminRole);
        $member->roles()->attach($memberRole);
    }
}
