@extends('layouts.master')

@section('breadcrumb')
Withdraw Request
@endsection

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Username</th>
                                <th class="text-center">Bank Name</th>
                                <th class="text-center">Account Number</th>
                                <th class="text-center">Account Holder</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">When</th>
                                <th class="text-center">Processed By</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td class="text-left">mid0ne</td>
                                <td class="text-left">BCA</td>
                                <td class="text-left">7838788373</td>
                                <td class="text-left">Midone</td>
                                <td class="text-right">200.000</td>
                                <td class="text-right">2020-12-02 00:12:02</td>
                                <td class="text-right">admin1</td>
                                <td class="text-center">
                                    <button class="btn btn-sm btn-inverse-secondary">Mistake</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection