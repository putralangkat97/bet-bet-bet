@extends('layouts.master')

@section('breadcrumb')
Member
@endsection

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-hover" id="order-listing">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-left">Name </th>
                                <th class="text-left">Email</th>
                                <th class="text-center">Bank Name</th>
                                <th class="text-center">Account Number</th>
                                <th class="text-center">Credit</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td>Miracle</td>
                                <td>aldamenkingt@gmail.com</td>
                                <td>BCA</td>
                                <td>865-023-9231</td>
                                <td>800,000</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-inverse-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Action</button>
                                            <button class="dropdown-item" type="button">Another action</button>
                                            <button class="dropdown-item" type="button">Something else here</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                               <td>2.</td>
                                <td>Dendi</td>
                                <td>dendinavi@gmail.com</td>
                                <td>BCA</td>
                                <td>099-007-2011</td>
                                <td>540,000</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-inverse-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Action</button>
                                            <button class="dropdown-item" type="button">Another action</button>
                                            <button class="dropdown-item" type="button">Something else here</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Sumail</td>
                                <td>sumail.bet@gmail.com</td>
                                <td>BCA</td>
                                <td>887-002-8221</td>
                                <td>435,000</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-inverse-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Action</button>
                                            <button class="dropdown-item" type="button">Another action</button>
                                            <button class="dropdown-item" type="button">Something else here</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection