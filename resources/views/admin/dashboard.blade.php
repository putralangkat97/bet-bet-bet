@extends('layouts.master')

@section('breadcrumb')
Dashboard
@endsection

@section('content')
<!-- Deposit -->
<div class="col-md-4 grid-margin">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                <h4 style="font-size: 20px;" class="card-title"><strong><i class="ti-angle-double-right text-primary"></i>&nbsp;&nbsp;Deposit Request</h4></strong>
            </div><hr>
            <!-- <p class="card-description">David Beckham</p> -->
            <div class="list d-flex align-items-center border-bottom py-3">
                <i class="fa fa-circle text-success"></i>
                <div class="wrapper w-100 ml-3">
                    <b>J. Zanneti</b>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <p class="mb-0">Amount: Rp. 450,000</p>
                        </div>
                        <small class="text-muted ml-auto">2 hours ago</small>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <p class="text-muted">Sender</p>
                <h4 style="margin-top: -15px;">J. Zanneti</h4>
                <p style="margin-top: -6px;">BCA / 234-234-2345</p>
                <hr>
                <p class="text-muted">Receiver</p>
                <h4 style="margin-top: -15px;">Aqua</h4>
                <p style="margin-top: -6px;">BCA / 456-456-4567</p>
                <div class="d-flex justify-content-between">
                    <a href="#" class="btn btn-inverse-danger"><i class="fa fa-times"></i> Reject</a>
                    <a href="#" class="btn btn-inverse-success"><i class="fa fa-check"></i> Accept</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Withdraw -->
<div class="col-md-4 grid-margin">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                <h4 style="font-size: 20px;" class="card-title"><strong><i class="ti-angle-double-up text-warning"></i>&nbsp;&nbsp;Withdraw Request</h4></strong>
            </div><hr>
            <!-- <p class="card-description">David Beckham</p> -->
            <div class="list d-flex align-items-center border-bottom py-3">
                <i class="fa fa-circle text-warning"></i>
                <div class="wrapper w-100 ml-3">
                    <b>Diego Milito</b>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <p class="mb-0">Amount: Rp. 350,000</p>
                        </div>
                        <small class="text-muted ml-auto">10 minutes ago</small>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <p class="text-muted">Receiver</p>
                <h4 style="margin-top: -15px;">Diego Milito</h4>
                <p style="margin-top: -6px;">BCA / 987-987-9876</p>
                <div class="d-flex justify-content-between">
                    <a href="#" class="btn btn-inverse-danger">Cancel <i class="fa fa-times"></i></a>
                    <a href="#" class="btn btn-inverse-success">Accept <i class="fa fa-check"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Lock Member -->
<div class="col-md-4 grid-margin">
    <a href="#" style="text-decoration: none;">
    <div class="card">
        <div class="card-body">
            <h4 style="font-size: 20px;" class="card-title"><strong><i class="fa fa-lock text-danger"></i>&nbsp;&nbsp;Lock Member Site</h4></strong>
        </div>
    </a>
    </div>

    <br>

    <a href="#" style="text-decoration: none;">
    <div class="card">
        <div class="card-body">
            <h4 style="font-size: 20px;" class="card-title"><strong><i class="fa fa-users text-primary"></i>&nbsp;&nbsp;Online Members (235)</h4></strong>
        </div>
    </a>
    </div>

    <br>

    <!-- Banks -->
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                <h4 style="font-size: 20px;" class="card-title"><strong><i class="icon-wallet text-success"></i>&nbsp;&nbsp;Bank Account</h4></strong>
            </div><hr>
            <!-- <p class="card-description">David Beckham</p> -->
            <div class="list d-flex align-items-center border-bottom py-3">
                <i style="font-size: 8px;" class="fa fa-circle fa-1x text-success"></i>
                <div class="wrapper w-100 ml-3">
                    <b>BCA</b>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <p class="mb-0">AQUA: 456-456-4567</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list d-flex align-items-center border-bottom py-3">
                <i style="font-size: 8px;" class="fa fa-circle fa-1x text-success"></i>
                <div class="wrapper w-100 ml-3">
                    <b>BNI</b>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <p class="mb-0">GALON: 6345-645-623</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list d-flex align-items-center border-bottom py-3">
                <i style="font-size: 8px;" class="fa fa-circle fa-1x text-success"></i>
                <div class="wrapper w-100 ml-3">
                    <b>BRI</b>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <p class="mb-0">HABIS: 456-456-456-764-524</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection