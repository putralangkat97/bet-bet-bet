@extends('layouts.master')

@section('breadcrumb')
Announcement
@endsection

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Striped Table</h4>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Time</th>
                            <th>Content</th>
                            <th>Create By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td>25 February 2020</td>
                            <td width="500px">Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, doloremque? Aut, et repudiandae. Beatae accusamus impedit delectus reprehenderit fugit quasi, incidunt aperiam nisi accusantium ullam nulla exercitationem ipsam qui! Porro veniam quos, saepe consequuntur soluta, praesentium, suscipit tempora necessitatibus eveniet voluptatem reiciendis molestias corrupti quas quibusdam ab blanditiis fugiat tenetur?</td>
                            <td>Polandia</td>
                            <td>
                                <input type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="outline-light">
                            </td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>29 February 2020</td>
                            <td width="500px">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error quae quibusdam ipsum nesciunt repudiandae sit iste veritatis. Fuga similique hic quia voluptas iure ipsa aliquam illum nesciunt, consectetur corrupti blanditiis sit quibusdam inventore quidem dolores quo doloribus, officiis provident nemo. Omnis illo ratione totam, tempora enim, modi, quas suscipit ad sapiente eum quo possimus saepe! Ducimus voluptatum sit fugit. Quaerat explicabo, dolorem beatae accusamus unde, non doloremque sint fugiat ducimus dolorum at culpa. Sit fugit ut nisi nobis itaque vel iusto aspernatur iste, enim deleniti vero. Tempora tenetur molestiae fugit quasi minus optio doloribus vel voluptatibus voluptate similique, deserunt inventore.</td>
                            <td>Polandia</td>
                            <td>
                                <input type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="outline-light">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection