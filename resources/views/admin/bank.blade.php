@extends('layouts.master')

@section('breadcrumb')
Manage Bank Account
@endsection

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Bank Name</th>
                                <th class="text-center">Account Holder</th>
                                <th class="text-center">Account Number</th>
                                <th class="text-left" width="230">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td class="text-left">BCA</td>
                                <td class="text-left">AQUA</td>
                                <td class="text-left">1120039292</td>
                                <td>
                                <i class="fa fa-circle text-success"></i> Online
                                    <div class="dropdown float-right">
                                        <button class="btn btn-inverse-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Action</button>
                                            <button class="dropdown-item" type="button">Another action</button>
                                            <button class="dropdown-item" type="button">Something else here</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td class="text-left">BNI</td>
                                <td class="text-left">AQUA</td>
                                <td class="text-left">0024339218</td>
                                <td>
                                <i class="fa fa-circle text-success"></i> Online
                                    <div class="dropdown float-right">
                                        <button class="btn btn-inverse-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Action</button>
                                            <button class="dropdown-item" type="button">Another action</button>
                                            <button class="dropdown-item" type="button">Something else here</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td class="text-left">BRI</td>
                                <td class="text-left">AQUA</td>
                                <td class="text-left">54521109239292</td>
                                <td>
                                <i class="fa fa-circle text-success"></i> Online
                                    <div class="dropdown float-right">
                                        <button class="btn btn-inverse-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Action</button>
                                            <button class="dropdown-item" type="button">Another action</button>
                                            <button class="dropdown-item" type="button">Something else here</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection