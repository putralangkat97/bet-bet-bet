@extends('layouts.master')

@section('breadcrumb')
Staff
@endsection

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Username</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Full Name</th>
                                <th class="text-center">Role</th>
                                <th class="text-center">Last Login</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1.</td>
                                <td class="text-left">mira.miracle</td>
                                <td class="text-left">aldamenkingt@gmail.com</td>
                                <td class="text-left">Amer Al-Barkawi</td>
                                <td class="text-left">superadmin</td>
                                <td class="text-right">2 hours ago</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Manage
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Change Password</button>
                                        </div>
                                    </div>
                                    <button class="btn btn-sm btn-success">Active</button>
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td class="text-left">mid0ne</td>
                                <td class="text-left">midonemindone@gmail.com</td>
                                <td class="text-left">Park Ji-Sung</td>
                                <td class="text-left">admin</td>
                                <td class="text-right">12 minutes ago</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Manage
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Change Password</button>
                                        </div>
                                    </div>
                                    <button class="btn btn-sm btn-success">Active</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection