<nav class="sidebar sidebar-offcanvas sidebar-dark" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <img src="{{ asset('chroma/images/faces/face23.jpg') }}" alt="profile image">
            <p class="text-center font-weight-medium">Lina</p>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin_zone">
              <i class="menu-icon icon-diamond"></i>
              <span class="menu-title">Dashboard</span>
              <!-- <div class="badge badge-success">3</div> -->
            </a>
          </li>
          <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="/admin_zone/announcement" aria-expanded="false" aria-controls="page-layouts">
              <i class="menu-icon icon-bell"></i>
              <span class="menu-title">Announcement</span>
            </a>
          </li>
          <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="/admin_zone/member" aria-expanded="false" aria-controls="page-layouts">
              <i class="menu-icon icon-people"></i>
              <span class="menu-title">Member Account</span>
            </a>
          </li>
          <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="/admin_zone/staff" aria-expanded="false" aria-controls="page-layouts">
              <i class="menu-icon icon-user"></i>
              <span class="menu-title">Staff Account</span>
            </a>
          </li>
          <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="/admin_zone/deposit" aria-expanded="false" aria-controls="page-layouts">
              <i class="menu-icon icon-arrow-down-circle"></i>
              <span class="menu-title">Deposit Request</span>
            </a>
          </li>
          <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="/admin_zone/withdraw" aria-expanded="false" aria-controls="page-layouts">
              <i class="menu-icon icon-arrow-up-circle"></i>
              <span class="menu-title">Withdraw Request</span>
            </a>
          </li>
          <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="/admin_zone/bank" aria-expanded="false" aria-controls="page-layouts">
              <i class="menu-icon icon-credit-card"></i>
              <span class="menu-title">Bank Account</span>
            </a>
          </li>
          <li class="nav-item d-none d-md-block">
            <a class="nav-link" href="/admin_zone/mutation" aria-expanded="false" aria-controls="page-layouts">
              <i class="menu-icon icon-wallet"></i>
              <span class="menu-title">Account Mutation</span>
            </a>
          </li>
        </ul>
      </nav>
